import { exercices } from './variables';

const formatDate = (date) => {
  const dateNumbers = date.split('-');
  return`${dateNumbers[2]}/${dateNumbers[1]}/${dateNumbers[0]}`;
}

const sortList = (list, column, growing) => (
  list.sort((a, b) => {
    const v1 = !Number.isNaN(+a[column]) ? +a[column] : a[column];
    const v2 = !Number.isNaN(+b[column]) ? +b[column] : b[column];
    if (v1 > v2) {
      return growing ? 1 : -1;
    }
    if (v1 < v2) {
      return growing ? -1 : 1;
    }
    return 0;
  })
);

const validateExerciceForm = (duration, type, date) => {
  const genericError = 'Erro na validação do formulario, favor verifique:\n';
  let error = '';

  if(duration < 1) {
    error+='- Duração deve ser maior que zero\n';
  }

  if(!exercices.find(exercice => exercice === type)) {
    error+= '- Modalidade Inválida\n';
  }

  if(!date.match(/([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/)) {
    error+= '- Formato de data inválido'
  }

  return error ? genericError + error : error;
}

export {
  formatDate,
  validateExerciceForm,
  sortList,
}
import React from 'react';
import { shallow, mount } from 'enzyme';

import ExerciceLog from '../../pages/ExerciceLog';
import Input from '../../components/Input';
import Dropdown from '../../components/Dropdown';
import { ExerciceForm } from '../../pages/ExerciceLog/styles';
import { exercices } from '../../variables';

jest.mock('../../components/ExerciceTable', () => ({ removeByIndex, orderByColumn }) => (
  <div>
    <button id='remove-by-index' onClick={() => removeByIndex(0)}>remove</button>
    <button id='order-by-column' onClick={() => orderByColumn('date')}>order</button>
    <button id='order-by-column-equal-column' onClick={() => orderByColumn('')}>order</button>
  </div>
))

describe('ExerciceLog', () => {
  const mock = (function() {
    let store = {};
    return {
      getItem: function(key) {
        return store[key];
      },
      setItem: function(key, value) {
        store[key] = value.toString();
      },
      clear: function() {
        store = {};
      }
    };
  })();

  const setState = jest.fn();
  const useStateSpy = jest.spyOn(React, 'useState')
  useStateSpy.mockImplementation((init) => [init, setState]);
  
  Object.defineProperty(window, 'localStorage', { 
    value: mock,
  });

  it('should render correctly', () => {
    const wrapper = shallow(<ExerciceLog/>);
    expect(wrapper).toMatchSnapshot();
  })

  it('call setState with restant list when click in removeByIndex', () => {
    const wrapper = mount(<ExerciceLog/>)
    expect(setState).toHaveBeenCalledWith('duration');
    setState.mockClear();
    wrapper.find('#remove-by-index').simulate('click');
    expect(setState).toHaveBeenCalledWith([]);
  });

  it('call setState with correct valuer when click in orderByColumn', () => {
    const wrapper = mount(<ExerciceLog/>)
    expect(setState).toHaveBeenCalledWith('duration');
    setState.mockClear();
    wrapper.find('#order-by-column').simulate('click');
    expect(setState).toHaveBeenCalledWith('date');
    expect(setState).toHaveBeenCalledWith(true);
  });

  it('call setState with correct value when click in orderByColumn with column equal than ordenationColumn', () => {
    const wrapper = mount(<ExerciceLog/>)
    expect(setState).toHaveBeenCalledWith('duration');
    setState.mockClear();
    wrapper.find('#order-by-column-equal-column').simulate('click');
    expect(setState).toHaveBeenCalledWith(false);
  });

  it('call setState with correct value when form was submitted with correct values', () => {
    const wrapper = mount(<ExerciceLog/>)
    const form = wrapper.find(ExerciceForm).find('form');
    expect(setState).toHaveBeenCalledWith('duration');
    wrapper.find(Input).at(0).find('input').instance().value = 10;
    wrapper.find(Input).at(1).find('input').instance().value = '2010-10-10';
    wrapper.find(Dropdown).at(0).find('select').instance().value = exercices[0];
    setState.mockClear();
    form.simulate('submit');
    expect(setState).toHaveBeenCalledWith([{ duration: '10', type: exercices[0], date: '2010-10-10' }]);
  });
});
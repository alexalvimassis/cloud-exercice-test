import React from 'react';
import { shallow } from 'enzyme';

import { TableRow, TableHeadItem } from '../../components/ExerciceTable/styles';
import ExerciceTable from '../../components/ExerciceTable';
import Button from '../../components/Button';

describe('ExerciceTable', () => {
  it('should render correctly', () => {
    const wrapper = shallow(
      <ExerciceTable
        removeByIndex={() => {}}
        orderByColumn={() => {}}
        ordenationColumn={'duration'}
        ordenationGrowing={true}
        list={[]}/>);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render correct number of rows', () => {
    const wrapper = shallow(
      <ExerciceTable
        removeByIndex={() => {}}
        orderByColumn={() => {}}
        ordenationColumn={'duration'}
        ordenationGrowing={true}
        list={[{ duration: '2', type: 'Test', date: '2010-10-10' }]}/>);
    expect(wrapper.find(TableRow)).toHaveLength(1);
  });

  it('should call removeByIndex when user click in remove button', () => {
    const mockedRemoveByIndex = jest.fn();
    const wrapper = shallow(
      <ExerciceTable
        removeByIndex={mockedRemoveByIndex}
        orderByColumn={() => {}}
        ordenationColumn={'duration'}
        ordenationGrowing={true}
        list={[{ duration: '2', type: 'Test', date: '2010-10-10' }]}/>);
    wrapper.find(TableRow).find(Button).simulate('click');
    expect(mockedRemoveByIndex).toHaveBeenCalledWith(0);
  });

  it('should call orderByColum when user click in header of table column', () => {
    const mockedOrderByColumn = jest.fn();
    const wrapper = shallow(
      <ExerciceTable
        removeByIndex={() => {}}
        orderByColumn={mockedOrderByColumn}
        ordenationColumn={'duration'}
        ordenationGrowing={true}
        list={[{ duration: '2', type: 'Test', date: '2010-10-10' }]}/>);
    wrapper.find(TableHeadItem).at(0).simulate('click');
    expect(mockedOrderByColumn).toHaveBeenCalledWith('duration');
  });
});
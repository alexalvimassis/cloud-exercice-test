import React from 'react';
import { shallow } from 'enzyme';

import Card from '../../components/Card';

describe('Card', () => {
  it('should render correctly', () => {
    const wrapper = shallow(<Card>Test</Card>);
    expect(wrapper).toMatchSnapshot();
  })
});
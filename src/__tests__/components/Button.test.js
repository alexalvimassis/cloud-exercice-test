import React from 'react';
import { shallow } from 'enzyme';

import Button from '../../components/Button';

describe('Button', () => {
  it('should render correctly', () => {
    const wrapper = shallow(<Button>Teste</Button>);
    expect(wrapper).toMatchSnapshot();
  })
});
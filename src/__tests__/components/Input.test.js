import React from 'react';
import { shallow } from 'enzyme';

import Input from '../../components/Input';

describe('Input', () => {
  it('should render correctly', () => {
    const wrapper = shallow(<Input label='Teste' type='text'/>);
    expect(wrapper).toMatchSnapshot();
  })
});
import React from 'react';
import { shallow } from 'enzyme';

import Dropdown from '../../components/Dropdown';

describe('Dropdown', () => {
  it('should render correctly', () => {
    const wrapper = shallow(<Dropdown label='Test' type='text' options={[1,2,3]}/>);
    expect(wrapper).toMatchSnapshot();
  })

  it('should render all options', () => {
    const wrapper = shallow(<Dropdown label='Test' type='text' options={[1,2,3]}/>);
    expect(wrapper.find('option')).toHaveLength(3);
  });

  it('should render all options with placeholder', () => {
    const wrapper = shallow(<Dropdown label='Test' placeholder='Test' type='text' options={[1,2,3]}/>);
    expect(wrapper.find('option')).toHaveLength(4);
  });
});
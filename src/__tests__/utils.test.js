import { formatDate, sortList, validateExerciceForm } from '../utils';
import { exercices } from '../variables';

describe('Utils tests', () => {
  describe('formatDate', () => {
    it('should return formated date', () => {
      const expectedResult = '20/10/2010';
      const mockedDate = '2010-10-20';
      expect(formatDate(mockedDate)).toEqual(expectedResult);
    });
  });

  describe('sortList', () => {
    it('should return a growing sorted list using column with numbers', () => {
      const mockedList = [
        { value: 10 },
        { value: 5 },
        { value: 20 },
      ];
      const expectedResult = [
        { value: 5 },
        { value: 10 },
        { value: 20 },
      ];

      expect(sortList(mockedList, 'value', true)).toEqual(expectedResult);
    });
    it('should return a decresing sorted list using column with strings', () => {
      const mockedList = [
        { value: 'Corrida' },
        { value: 'Natação' },
        { value: 'Bicicleta' },
      ];
      const expectedResult = [
        { value: 'Natação' },
        { value: 'Corrida' },
        { value: 'Bicicleta' },
      ];

      expect(sortList(mockedList, 'value', false)).toEqual(expectedResult);
    });
  });

  describe('validateExerciceForm', () => {
    it('should return an empty string when form is valid', () => {
      expect(validateExerciceForm(1, exercices[0], '2012-10-10'))
    });
    it('should return fields errors when values are invalids', () => {
      const expectedValues = 'Erro na validação do formulario, favor verifique:\n' +
       '- Duração deve ser maior que zero\n' +
       '- Modalidade Inválida\n' +
       '- Formato de data inválido';
      expect(validateExerciceForm(-1, 'Invalido', '202-18-32'))
    })
  });
});
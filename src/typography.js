export default {
  tiny: '12px',
  medium: '16px',
  large: '24px',
};

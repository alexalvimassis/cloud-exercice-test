import styled from 'styled-components';

import Colors from '../../colors';
import Dimensions from '../../dimensions';

const Card = styled.div`
  background-color: ${Colors.lightestColor};
  border: solid 1px ${Colors.lightGray};
  border-radius: ${Dimensions.baseRadius};
  box-shadow: 0 1px 2px rgba(0,0,0,.05);;
  margin: ${Dimensions.baseSpace} 0;
  padding: ${Dimensions.doubleSpace};
`;

export default Card;
import React from 'react';

import { StyledInput, LabelText, InputContainer } from './styles';

export default ({ label, ...args }) => (
  <InputContainer>
    <LabelText>{label}:</LabelText>
    <StyledInput {...args}/>
  </InputContainer>
);
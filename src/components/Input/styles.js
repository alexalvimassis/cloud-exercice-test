import styled from 'styled-components';

import Colors from '../../colors';
import Dimensions from '../../dimensions';
import Typography from '../../typography';

const LabelText = styled.span`
  color: ${Colors.darkGray};
  display: block;
  font-size: ${Typography.tiny};
  margin-bottom: ${Dimensions.tinySpace};
`;

const StyledInput = styled.input`
  background-color: ${Colors.lightestColor};
  border: solid 1px ${Colors.lightGray};
  border-radius: ${Dimensions.tinyRadius};
  color: ${Colors.darkGray};
  font-size: ${Typography.medium};
  padding: ${Dimensions.tinySpace} ${Dimensions.baseSpace};
  width: 100%;
`;

const InputContainer = styled.label`
  width: 100%;
`;

export {
  LabelText,
  StyledInput,
  InputContainer,
}
import styled from 'styled-components';

import Colors from '../../colors';
import Typography from '../../typography';
import Dimensions from '../../dimensions';

const Button = styled.button`
  background-color: ${({ danger }) => danger ? Colors.dangerColor : Colors.mainColor};
  border: none;
  border-radius: ${Dimensions.tinyRadius};
  color: ${Colors.lightestColor};
  cursor: pointer;
  font-size: ${Typography.medium};
  padding: ${Dimensions.tinySpace} ${Dimensions.doubleSpace};
`;

export default Button;
import React, { Fragment } from 'react';

import { StyledSelect, LabelText, InputContainer } from './styles';

export default ({ label, placeholder, options, ...args }) => (
  <Fragment>
    {options &&
      <InputContainer>
        <LabelText>{label}:</LabelText>
        <StyledSelect defaultValue='' {...args}>
          {placeholder && <option value='' disabled>{placeholder}</option>}
          {options.map((option, i) => <option key={i}>{option}</option>)}
        </StyledSelect>
      </InputContainer>}
    </Fragment>
);
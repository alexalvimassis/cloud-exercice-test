
import styled from 'styled-components';

import Colors from '../../colors';
import Dimensions from '../../dimensions';
import Typography from '../../typography';

const LabelText = styled.span`
  color: ${Colors.darkGray};
  display: block;
  font-size: ${Typography.tiny};
  margin-bottom: ${Dimensions.tinySpace};
`;

const StyledSelect = styled.select`
  background-color: ${Colors.lightestColor};
  border: solid 1px ${Colors.lightGray};
  border-radius: ${Dimensions.tinyRadius};
  color: ${Colors.darkGray};
  height: 32px;
  font-size: ${Typography.medium};
  width: 100%;
`;

const InputContainer = styled.label`
  width: 100%;
`;

export {
  LabelText,
  StyledSelect,
  InputContainer,
}
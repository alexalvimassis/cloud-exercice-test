import React, { Fragment } from 'react';

import Button from '../Button';
import { formatDate } from '../../utils';
import {
  Table,
  TableRow,
  TableHeadItem,
  Container,
} from './styles';

export default ({ list, removeByIndex, orderByColumn, ordenationColumn, ordenationGrowing }) => (
  <Fragment>
    {list && list.length > 0 ?
      <Container>
        <Table>
          <thead>
            <tr>
              <TableHeadItem
                active={ordenationColumn === 'duration'}
                onClick={() => orderByColumn('duration')}
                ordenationGrowing={ordenationGrowing}>Duração</TableHeadItem>
              <TableHeadItem
                active={ordenationColumn === 'type'}
                onClick={() => orderByColumn('type')}
                ordenationGrowing={ordenationGrowing}>Modalidade</TableHeadItem>
              <TableHeadItem
                active={ordenationColumn === 'date'}
                onClick={() => orderByColumn('date')}
                ordenationGrowing={ordenationGrowing}>Data</TableHeadItem>
              <TableHeadItem>Ações</TableHeadItem>
            </tr>
          </thead>
          <tbody>
            {list.map((item, i) =>
              <TableRow isGrey={i % 2 === 0} key={i}>
                <td>{item.duration} hr{item.duration > 1 && 's'}</td>
                <td>{item.type}</td>
                <td>{formatDate(item.date)}</td>
                <td>
                  <Button
                    onClick={() => removeByIndex(i)}
                    danger>
                    Excluir
                  </Button>
                </td>
              </TableRow>)}
          </tbody>
        </Table>
      </Container>: <div>Não existem exercícios cadastrados</div>}
    </Fragment>
)
import styled from 'styled-components';

import Dimensions from '../../dimensions';
import Colors from '../../colors';

const Table = styled.table`
  border-spacing: 0;
  width: 100%;

  td, th {
    padding: ${Dimensions.doubleSpace} ${Dimensions.baseSpace};
  }

  thead {
    border-bottom: solid 2px ${Colors.mainColor};
  }
`;

const TableRow = styled.tr`
  ${({ isGrey }) => isGrey && `background-color: ${Colors.lightGray};`}

  color: ${Colors.darkGray};
  text-align: center;
`;

const TableHeadItem = styled.th`
  color: ${Colors.darkestColor};
  cursor: pointer;
  font-weight: bold;

  ${({ active, ordenationGrowing }) =>
    active &&
    `
      &:after {
        ${ordenationGrowing ?
        `border-top: solid 8px ${Colors.darkestColor};` :
        `border-bottom: solid 8px ${Colors.darkestColor};`}
        content: '';
        border-right: solid 8px transparent;
        border-left: solid 8px transparent;
        display: inline-block;
        margin-left: ${Dimensions.tinySpace};
      }
    `
  }
`;

const Container = styled.div`
  border: solid 1px ${Colors.lightGray};
  overflow: auto;
`;

export {
  Table,
  TableRow,
  TableHeadItem,
  Container,
}
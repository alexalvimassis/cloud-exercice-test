import styled from 'styled-components';

import Dimensions from '../../dimensions';
import Typography from '../../typography';
import Colors from '../../colors';

const Container = styled.div`
  margin: 0 auto;
  max-width: 900px;
  width: 90%;
`;

const PageTitle = styled.h1`
  color: ${Colors.darkestColor};
  font-size: ${Typography.large};
  margin: ${Dimensions.largeSpace} 0;
  text-align: center;

  &:after {
    border-bottom: 4px solid ${Colors.mainColor};
    content: '';
    display: block;
    margin: ${Dimensions.doubleSpace} auto 0px;
    width: 96px;
  }
`;

const ExerciceForm = styled.form`
  display: flex;
  flex-direction: column;

  label {
    display: block;
    margin-bottom: ${Dimensions.baseSpace};
  }

  button {
    align-self: flex-end;
  }
`;

const TotalDurationText = styled.span`
  color: ${Colors.darkestColor};
  display: block;
  font-size: ${Typography.large};
  margin-top: ${Dimensions.doubleSpace};
  text-align: center;
`;

export {
  Container,
  PageTitle,
  ExerciceForm,
  TotalDurationText,
}
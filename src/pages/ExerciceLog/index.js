import React, { useState, useEffect } from 'react';

import { exercices } from '../../variables';
import { validateExerciceForm, sortList } from '../../utils';
import Card from '../../components/Card';
import Input from '../../components/Input';
import Button from '../../components/Button';
import DropDown from '../../components/Dropdown';
import ExerciceTable from '../../components/ExerciceTable';
import {
  Container,
  PageTitle,
  ExerciceForm,
  TotalDurationText,
} from './styles';

export default () =>  {
  const [list, setList] = useState([])
  const [ordenationColumn, setOrdenationColumn] = useState('');
  const [ordenationGrowing, setOrdenationGrowing] = useState(true)

  useEffect(() => {
    const localList = localStorage.getItem('cloud-exercice-test-list');
    if(localList) {
      setList(JSON.parse(localList));
    }
    setOrdenationColumn('duration');
  }, []);

  useEffect(() => {
    localStorage.setItem('cloud-exercice-test-list', JSON.stringify(list));
  }, [list])

  useEffect(() => {
    const sortedList = sortList(list, ordenationColumn, ordenationGrowing);
    
    if(list && list.length > 0) {
      setList([...sortedList]);
    }
  }, [ordenationGrowing, ordenationColumn])

  const handleOnSubmit = (evt) => {
    evt.preventDefault();
    const duration = evt.target.elements[0].value;
    const type = evt.target.elements[1].value;
    const date = evt.target.elements[2].value;
    const errorMessage = validateExerciceForm(duration, type, date);

    if(!errorMessage) {
      evt.target.reset();
      setList([
        ...sortList([...list, { duration, type, date }], ordenationColumn, ordenationGrowing)
      ]);
    } else {
      alert(errorMessage);
    }
  }

  const removeByIndex = (index) => {
    setList(list.filter((_, i) => i !== index));
  }

  const orderByColumn = (column) => {
    if(column === ordenationColumn) {
      setOrdenationGrowing(!ordenationGrowing);
    } else {
      setOrdenationColumn(column);
      setOrdenationGrowing(true);
    }
  }
  const totalDuration = list.reduce((acc, item) => acc + +item.duration,0);

  return (
    <Container>
      <PageTitle>Log de Exercícios</PageTitle>
      <Card>
        <ExerciceForm onSubmit={handleOnSubmit}>
          <Input
            label='Duração (em horas)'
            type='number'
            required/>
          <DropDown
            options={exercices}
            label='Modalidade'
            placeholder='Selecione aqui a modalidade'
            required/>
          <Input
            label='Data'
            type='date'
            required/>
          <Button type='submit'>
            Adicionar
          </Button>
        </ExerciceForm>
      </Card>
      <Card>
        <ExerciceTable
          removeByIndex={removeByIndex}
          orderByColumn={orderByColumn}
          ordenationColumn={ordenationColumn}
          ordenationGrowing={ordenationGrowing}
          list={list}/>
        {totalDuration > 0 &&
          <TotalDurationText>Você já fez {totalDuration} hora{totalDuration > 1 && 's'} de exercício</TotalDurationText>}
      </Card>
    </Container>
  );
};

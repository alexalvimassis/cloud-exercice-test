import React from 'react';
import ReactDOM from 'react-dom';
import ExerciceLog from './pages/ExerciceLog';
import './reset.css';

ReactDOM.render(
  <ExerciceLog />,
  document.getElementById('root')
);

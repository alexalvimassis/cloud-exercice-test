export default {
  baseSpace: '8px',
  tinySpace: '4px',
  doubleSpace: '16px',
  largeSpace: '24px',

  baseRadius: '8px',
  tinyRadius: '3px',
};

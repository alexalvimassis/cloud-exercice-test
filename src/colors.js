export default {
  darkestColor: '#000',
  lightestColor: '#FFF',
  mainColor: '#1B05FF',
  darkGray: '#1D2129',
  lightGray: '#CDCDCD',
  dangerColor: '#E63D4A',
};

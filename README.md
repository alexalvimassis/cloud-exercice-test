## Activies Logger

That application has the responsibility to allow the user to register physical activities practiced in a day informing the hours dedicated, the date and the kind of the activity. After registered and stored locally these activities are shown in a list just below the register form allowing different ways of ordering and permitting the exclusion of some activity

### `npm install`

Install project dependencies

### `npm start`

Run the application in developer mode in port 3000.

### `npm test`

Run tests in `__tests__` folder inside src.

### `npm run build`

Create `build` folder for send to production.
